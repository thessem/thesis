KinDiff = [[(sin(phi_B)*sin(psi_B) + sin(theta_B)*cos(phi_B)*cos(psi_B))*w_B + (sin(phi_B)*sin(theta_B)*cos(psi_B) - sin(psi_B)*cos(phi_B))*v_B + u_B*cos(psi_B)*cos(theta_B)]
 [(-sin(phi_B)*cos(psi_B) + sin(psi_B)*sin(theta_B)*cos(phi_B))*w_B + (sin(phi_B)*sin(psi_B)*sin(theta_B) + cos(phi_B)*cos(psi_B))*v_B + u_B*sin(psi_B)*cos(theta_B)]
 [-u_B*sin(theta_B) + v_B*sin(phi_B)*cos(theta_B) + w_B*cos(phi_B)*cos(theta_B)]
 [p_B + q_B*sin(phi_B)*tan(theta_B) + r_B*cos(phi_B)*tan(theta_B)]
 [q_B*cos(phi_B) - r_B*sin(phi_B)]
 [(q_B*sin(phi_B) + r_B*cos(phi_B))/cos(theta_B)]];