function F = F(in1,in2,in3,in4)
%F
%    F = F(IN1,IN2,IN3,IN4)

%    This function was generated by the Symbolic Math Toolbox version 6.2.
%    15-Sep-2015 19:17:16

A_S = in4(:,9);
C_D0_B = in4(:,13);
C_D_S = in4(:,12);
C_Dalpha_B = in4(:,15);
C_L0_B = in4(:,14);
C_Lalpha_B = in4(:,16);
C_l_p = in4(:,17);
C_m_0 = in4(:,19);
C_m_q = in4(:,18);
C_n_r = in4(:,20);
C_t = in4(:,11);
I_Bxx = in4(:,6);
I_Byy = in4(:,7);
I_Bzz = in4(:,8);
I_Sxx = in4(:,3);
I_Syy = in4(:,4);
I_Szz = in4(:,5);
p_B = in3(:,4);
phi_B = in2(:,4);
q_B = in3(:,5);
r_B = in3(:,6);
roll = in1(:,2);
rolld = in1(:,3);
rolldd = in1(:,4);
theta_B = in2(:,5);
theta_S = in4(:,10);
throttle = in1(:,1);
u_B = in3(:,1);
v_B = in3(:,2);
w_B = in3(:,3);
z_MS = in4(:,2);
z_SC = in4(:,1);
t2 = u_B.^2;
t3 = w_B.^2;
t53 = w_B.*1i;
t54 = t53+u_B;
t4 = angle(t54);
t5 = v_B.^2;
t6 = t2+t3+t5;
t7 = sqrt(t6);
t8 = roll.*3.490658503988659e-1;
t9 = cos(t8);
t10 = sin(theta_S);
t11 = r_B.^2;
t12 = z_SC.^2;
t13 = rolld.^2;
t14 = p_B.^2;
t15 = q_B.^2;
t16 = t9.^2;
t17 = cos(theta_S);
t18 = t17.^2;
t19 = sin(t8);
t20 = t12.*t14;
t21 = t11.*t12;
t22 = t12.*t13.*1.218469679146834e-1;
t23 = p_B.*v_B.*-2.79;
t24 = q_B.*u_B.*2.79;
t25 = t14.*1.946025;
t26 = t15.*1.946025;
t27 = t12.*t14.*t16.*-1.0;
t28 = t12.*t15.*t16;
t29 = r_B.*rolld.*t10.*t12.*-6.981317007977318e-1;
t30 = t9.*t14.*t17.*z_SC.*2.79;
t31 = t9.*t15.*t17.*z_SC.*2.79;
t32 = t12.*t14.*t16.*t18;
t33 = t11.*t12.*t16.*t18.*-1.0;
t34 = p_B.*rolld.*t9.*z_SC.*9.738937226128359e-1;
t35 = rolld.*t9.*v_B.*z_SC.*-6.981317007977318e-1;
t36 = q_B.*r_B.*t19.*z_SC.*2.79;
t37 = p_B.*t19.*w_B.*z_SC.*-2.0;
t38 = r_B.*t19.*u_B.*z_SC.*2.0;
t39 = p_B.*rolld.*t12.*t17.*6.981317007977318e-1;
t40 = p_B.*t9.*t17.*v_B.*z_SC.*-2.0;
t41 = q_B.*t9.*t17.*u_B.*z_SC.*2.0;
t42 = p_B.*r_B.*t9.*t10.*z_SC.*-2.79;
t43 = q_B.*t9.*t10.*w_B.*z_SC.*-2.0;
t44 = r_B.*t9.*t10.*v_B.*z_SC.*2.0;
t45 = rolld.*t17.*t19.*w_B.*z_SC.*-6.981317007977318e-1;
t46 = q_B.*rolld.*t10.*t19.*z_SC.*-9.738937226128359e-1;
t47 = rolld.*t10.*t19.*u_B.*z_SC.*-6.981317007977318e-1;
t48 = q_B.*r_B.*t9.*t12.*t17.*t19.*2.0;
t49 = p_B.*q_B.*t9.*t10.*t12.*t19.*2.0;
t50 = p_B.*r_B.*t10.*t12.*t16.*t17.*-2.0;
t51 = t2+t3+t5+t20+t21+t22+t23+t24+t25+t26+t27+t28+t29+t30+t31+t32+t33+t34+t35+t36+t37+t38+t39+t40+t41+t42+t43+t44+t45+t46+t47+t48+t49+t50;
t52 = sqrt(t51);
t55 = t2+t3;
t56 = sqrt(t55);
t57 = t4.^2;
t58 = cos(theta_B);
t59 = sin(phi_B);
t60 = cos(phi_B);
t61 = sin(theta_B);
t62 = theta_S.*2.0;
t63 = sin(t62);
t64 = roll.*6.981317007977318e-1;
t65 = sin(t64);
t66 = t60.^2;
t67 = t66.^2;
F = [t61.*-2.46231e1-p_B.*r_B.*3.2085-q_B.*w_B.*2.51+r_B.*v_B.*2.51+C_t.*t17.*throttle-C_D0_B.*t7.*u_B.*2.9161125e-1+C_L0_B.*t56.*w_B.*2.9161125e-1-C_Dalpha_B.*t7.*t57.*u_B.*2.9161125e-1+C_Lalpha_B.*t4.*t56.*w_B.*2.9161125e-1+p_B.*q_B.*t19.*z_SC.*2.3-r_B.*rolld.*t9.*z_SC.*1.605702911834783+rolldd.*t10.*t19.*z_SC.*8.028514559173916e-1+t9.*t10.*t11.*z_SC.*2.3+t9.*t10.*t13.*z_SC.*2.802480262037719e-1+t9.*t10.*t15.*z_SC.*2.3-A_S.*C_D_S.*q_B.*t52.*8.544375e-1-A_S.*C_D_S.*t52.*u_B.*6.125e-1-p_B.*r_B.*t9.*t17.*z_SC.*2.3+q_B.*rolld.*t17.*t19.*z_SC.*1.605702911834783-A_S.*C_D_S.*r_B.*t19.*t52.*z_SC.*6.125e-1-A_S.*C_D_S.*q_B.*t9.*t17.*t52.*z_SC.*6.125e-1+A_S.*C_D_S.*rolld.*t10.*t19.*t52.*z_SC.*2.138028333693054e-1;q_B.*r_B.*-3.2085+p_B.*w_B.*2.51-r_B.*u_B.*2.51+t58.*t59.*2.46231e1-C_D0_B.*t7.*v_B.*2.9161125e-1+rolldd.*t9.*z_SC.*8.028514559173916e-1-t11.*t19.*z_SC.*2.3-t13.*t19.*z_SC.*2.802480262037719e-1-t14.*t19.*z_SC.*2.3-C_Dalpha_B.*t7.*t57.*v_B.*2.9161125e-1+A_S.*C_D_S.*p_B.*t52.*8.544375e-1-A_S.*C_D_S.*t52.*v_B.*6.125e-1-p_B.*q_B.*t9.*t10.*z_SC.*2.3-p_B.*rolld.*t17.*t19.*z_SC.*1.605702911834783-q_B.*r_B.*t9.*t17.*z_SC.*2.3+r_B.*rolld.*t10.*t19.*z_SC.*1.605702911834783+A_S.*C_D_S.*rolld.*t9.*t52.*z_SC.*2.138028333693054e-1+A_S.*C_D_S.*p_B.*t9.*t17.*t52.*z_SC.*6.125e-1-A_S.*C_D_S.*r_B.*t9.*t10.*t52.*z_SC.*6.125e-1;t14.*3.2085+t15.*3.2085-p_B.*v_B.*2.51+q_B.*u_B.*2.51+t58.*t60.*2.46231e1-C_t.*t10.*throttle.*1.0-C_L0_B.*t56.*u_B.*2.9161125e-1-C_D0_B.*t7.*w_B.*2.9161125e-1-C_Lalpha_B.*t4.*t56.*u_B.*2.9161125e-1-C_Dalpha_B.*t7.*t57.*w_B.*2.9161125e-1+p_B.*rolld.*t9.*z_SC.*1.605702911834783+q_B.*r_B.*t19.*z_SC.*2.3+rolldd.*t17.*t19.*z_SC.*8.028514559173916e-1+t9.*t13.*t17.*z_SC.*2.802480262037719e-1+t9.*t14.*t17.*z_SC.*2.3+t9.*t15.*t17.*z_SC.*2.3-A_S.*C_D_S.*t52.*w_B.*6.125e-1-p_B.*r_B.*t9.*t10.*z_SC.*2.3-q_B.*rolld.*t10.*t19.*z_SC.*1.605702911834783+A_S.*C_D_S.*p_B.*t19.*t52.*z_SC.*6.125e-1+A_S.*C_D_S.*q_B.*t9.*t10.*t52.*z_SC.*6.125e-1+A_S.*C_D_S.*rolld.*t17.*t19.*t52.*z_SC.*2.138028333693054e-1;q_B.*r_B.*4.4758575-p_B.*w_B.*3.2085+r_B.*u_B.*3.2085-t58.*t59.*3.1475385e1+C_l_p.*p_B.*t7.*3.1238126128125e-1+I_Byy.*q_B.*r_B-I_Bzz.*q_B.*r_B.*1.0-I_Sxx.*q_B.*r_B.*1.0+I_Szz.*q_B.*r_B-I_Sxx.*rolldd.*t17.*3.490658503988659e-1-q_B.*r_B.*t12.*2.3-rolldd.*t12.*t17.*8.028514559173916e-1-rolldd.*t9.*z_SC.*1.119977781004761+t11.*t19.*z_SC.*3.2085+t13.*t19.*z_SC.*3.909459965542618e-1-t15.*t19.*z_SC.*3.2085+I_Sxx.*q_B.*r_B.*t18+I_Syy.*q_B.*r_B.*t16-I_Syy.*q_B.*r_B.*t18.*1.0-I_Szz.*q_B.*r_B.*t16.*1.0+I_Sxx.*q_B.*rolld.*t10.*3.490658503988659e-1+I_Syy.*q_B.*rolld.*t10.*3.490658503988659e-1-I_Szz.*q_B.*rolld.*t10.*3.490658503988659e-1+q_B.*r_B.*t12.*t16.*2.3+q_B.*rolld.*t10.*t12.*1.605702911834783+p_B.*t19.*v_B.*z_SC.*2.3-q_B.*t19.*u_B.*z_SC.*2.3-t19.*t58.*t60.*z_SC.*2.2563e1-A_S.*C_D_S.*p_B.*t52.*1.1919403125+A_S.*C_D_S.*t52.*v_B.*8.544375e-1-A_S.*C_D_S.*p_B.*t12.*t52.*6.125e-1+I_Sxx.*p_B.*q_B.*t10.*t17-I_Syy.*p_B.*q_B.*t10.*t17.*1.0-I_Syy.*p_B.*rolld.*t9.*t19.*6.981317007977318e-1+I_Szz.*p_B.*rolld.*t9.*t19.*6.981317007977318e-1+I_Syy.*q_B.*r_B.*t16.*t18-I_Szz.*q_B.*r_B.*t16.*t18.*1.0-I_Syy.*q_B.*rolld.*t10.*t16.*6.981317007977318e-1+I_Szz.*q_B.*rolld.*t10.*t16.*6.981317007977318e-1+I_Syy.*t9.*t11.*t17.*t19-I_Szz.*t9.*t11.*t17.*t19.*1.0-I_Syy.*t9.*t15.*t17.*t19.*1.0+I_Szz.*t9.*t15.*t17.*t19+C_t.*t10.*t19.*throttle.*z_MS+C_t.*t10.*t19.*throttle.*z_SC-p_B.*rolld.*t9.*t12.*t19.*1.605702911834783+q_B.*r_B.*t12.*t16.*t18.*2.3-q_B.*rolld.*t10.*t12.*t16.*1.605702911834783+t9.*t11.*t12.*t17.*t19.*2.3-t9.*t12.*t15.*t17.*t19.*2.3+p_B.*q_B.*t9.*t10.*z_SC.*3.2085+p_B.*rolld.*t17.*t19.*z_SC.*2.239955562009523+q_B.*r_B.*t9.*t17.*z_SC.*6.417-r_B.*rolld.*t10.*t19.*z_SC.*2.239955562009523-p_B.*t9.*t17.*w_B.*z_SC.*2.3+r_B.*t9.*t17.*u_B.*z_SC.*2.3-t9.*t17.*t58.*t59.*z_SC.*2.2563e1+p_B.*q_B.*t10.*t12.*t16.*t17.*2.3+p_B.*r_B.*t9.*t10.*t12.*t19.*2.3+p_B.*rolld.*t9.*t12.*t18.*t19.*1.605702911834783+A_S.*C_D_S.*p_B.*t12.*t16.*t52.*6.125e-1-A_S.*C_D_S.*rolld.*t12.*t17.*t52.*2.138028333693054e-1-A_S.*C_D_S.*rolld.*t9.*t52.*z_SC.*2.98254952550181e-1+A_S.*C_D_S.*t19.*t52.*w_B.*z_SC.*6.125e-1+I_Syy.*p_B.*q_B.*t10.*t16.*t17-I_Szz.*p_B.*q_B.*t10.*t16.*t17.*1.0+I_Syy.*p_B.*r_B.*t9.*t10.*t19-I_Szz.*p_B.*r_B.*t9.*t10.*t19.*1.0+I_Syy.*p_B.*rolld.*t9.*t18.*t19.*6.981317007977318e-1-I_Szz.*p_B.*rolld.*t9.*t18.*t19.*6.981317007977318e-1-A_S.*C_D_S.*p_B.*t12.*t16.*t18.*t52.*6.125e-1-A_S.*C_D_S.*p_B.*t9.*t17.*t52.*z_SC.*1.708875+A_S.*C_D_S.*r_B.*t9.*t10.*t52.*z_SC.*8.544375e-1+A_S.*C_D_S.*t9.*t17.*t52.*v_B.*z_SC.*6.125e-1-I_Syy.*r_B.*rolld.*t9.*t10.*t17.*t19.*6.981317007977318e-1+I_Szz.*r_B.*rolld.*t9.*t10.*t17.*t19.*6.981317007977318e-1-r_B.*rolld.*t9.*t10.*t12.*t17.*t19.*1.605702911834783-A_S.*C_D_S.*q_B.*t9.*t10.*t12.*t19.*t52.*6.125e-1+A_S.*C_D_S.*r_B.*t10.*t12.*t16.*t17.*t52.*6.125e-1;t61.*-3.1475385e1+C_m_0.*t2.*9.914782499999999e-2+C_m_0.*t3.*9.914782499999999e-2+C_m_0.*t5.*9.914782499999999e-2-p_B.*r_B.*4.4758575-q_B.*w_B.*3.2085+r_B.*v_B.*3.2085+C_m_q.*q_B.*t7.*3.37102605e-2+C_t.*t17.*throttle.*1.395-I_Bxx.*p_B.*r_B.*1.0+I_Bzz.*p_B.*r_B+I_Sxx.*p_B.*r_B-I_Syy.*p_B.*r_B.*1.0+I_Sxx.*t11.*t63.*5.0e-1-I_Syy.*t11.*t63.*5.0e-1-I_Sxx.*t14.*t63.*5.0e-1+I_Syy.*t14.*t63.*5.0e-1+I_Syy.*q_B.*rolld.*t65.*3.490658503988659e-1-I_Szz.*q_B.*rolld.*t65.*3.490658503988659e-1-I_Sxx.*r_B.*rolld.*t17.*3.490658503988659e-1+I_Syy.*r_B.*rolld.*t17.*3.490658503988659e-1-I_Szz.*r_B.*rolld.*t17.*3.490658503988659e-1+C_t.*t9.*throttle.*z_MS+C_t.*t9.*throttle.*z_SC+p_B.*r_B.*t12.*t16.*2.3+q_B.*rolld.*t12.*t65.*8.028514559173916e-1+p_B.*q_B.*t19.*z_SC.*3.2085-r_B.*rolld.*t9.*z_SC.*2.239955562009523+rolldd.*t10.*t19.*z_SC.*1.119977781004761+t9.*t10.*t11.*z_SC.*3.2085+t9.*t10.*t13.*z_SC.*3.909459965542618e-1-t9.*t10.*t14.*z_SC.*3.2085-t9.*t17.*t61.*z_SC.*2.2563e1-A_S.*C_D_S.*q_B.*t52.*1.1919403125-A_S.*C_D_S.*t52.*u_B.*8.544375e-1-I_Sxx.*p_B.*r_B.*t18.*2.0+I_Syy.*p_B.*r_B.*t16+I_Syy.*p_B.*r_B.*t18.*2.0-I_Szz.*p_B.*r_B.*t16.*1.0-I_Sxx.*p_B.*rolld.*t10.*3.490658503988659e-1+I_Syy.*p_B.*rolld.*t10.*3.490658503988659e-1-I_Szz.*p_B.*rolld.*t10.*3.490658503988659e-1-I_Syy.*p_B.*r_B.*t16.*t18.*2.0+I_Szz.*p_B.*r_B.*t16.*t18.*2.0-I_Syy.*p_B.*rolld.*t10.*t16.*6.981317007977318e-1+I_Szz.*p_B.*rolld.*t10.*t16.*6.981317007977318e-1-I_Syy.*r_B.*rolld.*t16.*t17.*6.981317007977318e-1+I_Szz.*r_B.*rolld.*t16.*t17.*6.981317007977318e-1+I_Sxx.*r_B.*rolld.*t17.*t66.*1.0e-33-I_Sxx.*r_B.*rolld.*t17.*t67.*1.0e-33-I_Syy.*r_B.*rolld.*t17.*t66.*1.0e-33+I_Syy.*r_B.*rolld.*t17.*t67.*1.0e-33+I_Szz.*r_B.*rolld.*t17.*t66.*1.0e-33-I_Szz.*r_B.*rolld.*t17.*t67.*1.0e-33+I_Syy.*t10.*t11.*t16.*t17-I_Szz.*t10.*t11.*t16.*t17.*1.0-I_Syy.*t10.*t14.*t16.*t17.*1.0+I_Szz.*t10.*t14.*t16.*t17-p_B.*r_B.*t12.*t16.*t18.*4.6-p_B.*rolld.*t10.*t12.*t16.*1.605702911834783-r_B.*rolld.*t12.*t16.*t17.*1.605702911834783+t10.*t11.*t12.*t16.*t17.*2.3-t10.*t12.*t14.*t16.*t17.*2.3-p_B.*r_B.*t9.*t17.*z_SC.*6.417+q_B.*rolld.*t17.*t19.*z_SC.*2.239955562009523+r_B.*rolld.*t9.*t66.*z_SC.*5.0e-33-r_B.*rolld.*t9.*t67.*z_SC.*5.0e-33+p_B.*t9.*t10.*v_B.*z_SC.*2.3-q_B.*t9.*t10.*u_B.*z_SC.*2.3-t9.*t10.*t58.*t60.*z_SC.*2.2563e1-q_B.*t9.*t17.*w_B.*z_SC.*2.3+r_B.*t9.*t17.*v_B.*z_SC.*2.3+p_B.*q_B.*t9.*t12.*t17.*t19.*2.3-q_B.*r_B.*t9.*t10.*t12.*t19.*2.3-q_B.*rolld.*t17.*t19.*t66.*z_SC.*5.0e-33+q_B.*rolld.*t17.*t19.*t67.*z_SC.*5.0e-33-A_S.*C_D_S.*q_B.*t12.*t16.*t52.*6.125e-1-A_S.*C_D_S.*r_B.*t19.*t52.*z_SC.*8.544375e-1+I_Syy.*p_B.*q_B.*t9.*t17.*t19-I_Szz.*p_B.*q_B.*t9.*t17.*t19.*1.0-I_Syy.*q_B.*r_B.*t9.*t10.*t19.*1.0+I_Szz.*q_B.*r_B.*t9.*t10.*t19+I_Syy.*r_B.*rolld.*t16.*t17.*t66.*2.0e-33-I_Syy.*r_B.*rolld.*t16.*t17.*t67.*2.0e-33-I_Szz.*r_B.*rolld.*t16.*t17.*t66.*2.0e-33+I_Szz.*r_B.*rolld.*t16.*t17.*t67.*2.0e-33-A_S.*C_D_S.*q_B.*t9.*t17.*t52.*z_SC.*1.708875+A_S.*C_D_S.*rolld.*t10.*t19.*t52.*z_SC.*2.98254952550181e-1-A_S.*C_D_S.*t9.*t17.*t52.*u_B.*z_SC.*6.125e-1+A_S.*C_D_S.*t9.*t10.*t52.*w_B.*z_SC.*6.125e-1-A_S.*C_D_S.*p_B.*t9.*t10.*t12.*t19.*t52.*6.125e-1-A_S.*C_D_S.*r_B.*t9.*t12.*t17.*t19.*t52.*6.125e-1;C_n_r.*r_B.*t7.*3.1238126128125e-1+I_Bxx.*p_B.*q_B-I_Byy.*p_B.*q_B.*1.0+I_Syy.*p_B.*q_B-I_Szz.*p_B.*q_B.*1.0+I_Sxx.*rolldd.*t10.*3.490658503988659e-1+p_B.*q_B.*t12.*2.3+rolldd.*t10.*t12.*8.028514559173916e-1-t19.*t61.*z_SC.*2.2563e1+I_Sxx.*q_B.*rolld.*t17.*3.490658503988659e-1+I_Syy.*q_B.*rolld.*t17.*3.490658503988659e-1-I_Szz.*q_B.*rolld.*t17.*3.490658503988659e-1-p_B.*q_B.*t12.*t16.*4.6+q_B.*rolld.*t12.*t17.*1.605702911834783-p_B.*r_B.*t19.*z_SC.*3.2085-q_B.*t19.*w_B.*z_SC.*2.3+r_B.*t19.*v_B.*z_SC.*2.3+I_Sxx.*p_B.*q_B.*t18-I_Syy.*p_B.*q_B.*t16.*2.0-I_Syy.*p_B.*q_B.*t18.*1.0+I_Szz.*p_B.*q_B.*t16.*2.0-A_S.*C_D_S.*r_B.*t12.*t52.*6.125e-1+I_Syy.*p_B.*q_B.*t16.*t18-I_Szz.*p_B.*q_B.*t16.*t18.*1.0-I_Sxx.*q_B.*r_B.*t10.*t17.*1.0+I_Syy.*q_B.*r_B.*t10.*t17-I_Syy.*q_B.*rolld.*t16.*t17.*6.981317007977318e-1+I_Szz.*q_B.*rolld.*t16.*t17.*6.981317007977318e-1+I_Sxx.*q_B.*rolld.*t17.*t66.*1.25e-33-I_Sxx.*q_B.*rolld.*t17.*t67.*1.25e-33+I_Syy.*q_B.*rolld.*t17.*t66.*1.25e-33-I_Syy.*q_B.*rolld.*t17.*t67.*1.25e-33-I_Szz.*q_B.*rolld.*t17.*t66.*1.25e-33+I_Szz.*q_B.*rolld.*t17.*t67.*1.25e-33-I_Syy.*t9.*t10.*t14.*t19.*1.0+I_Syy.*t9.*t10.*t15.*t19+I_Szz.*t9.*t10.*t14.*t19-I_Szz.*t9.*t10.*t15.*t19.*1.0+C_t.*t17.*t19.*throttle.*z_MS+C_t.*t17.*t19.*throttle.*z_SC+p_B.*q_B.*t12.*t16.*t18.*2.3-q_B.*rolld.*t12.*t16.*t17.*1.605702911834783-t9.*t10.*t12.*t14.*t19.*2.3+t9.*t10.*t12.*t15.*t19.*2.3-q_B.*r_B.*t9.*t10.*z_SC.*3.2085+p_B.*t9.*t10.*w_B.*z_SC.*2.3-r_B.*t9.*t10.*u_B.*z_SC.*2.3+t9.*t10.*t58.*t59.*z_SC.*2.2563e1-p_B.*r_B.*t9.*t12.*t17.*t19.*2.3-q_B.*r_B.*t10.*t12.*t16.*t17.*2.3-r_B.*rolld.*t9.*t12.*t18.*t19.*1.605702911834783+A_S.*C_D_S.*rolld.*t10.*t12.*t52.*2.138028333693054e-1-A_S.*C_D_S.*q_B.*t19.*t52.*z_SC.*8.544375e-1-A_S.*C_D_S.*t19.*t52.*u_B.*z_SC.*6.125e-1-I_Syy.*p_B.*r_B.*t9.*t17.*t19.*1.0+I_Szz.*p_B.*r_B.*t9.*t17.*t19-I_Syy.*q_B.*r_B.*t10.*t16.*t17.*1.0+I_Szz.*q_B.*r_B.*t10.*t16.*t17-I_Syy.*q_B.*rolld.*t16.*t17.*t66.*2.5e-33+I_Syy.*q_B.*rolld.*t16.*t17.*t67.*2.5e-33+I_Szz.*q_B.*rolld.*t16.*t17.*t66.*2.5e-33-I_Szz.*q_B.*rolld.*t16.*t17.*t67.*2.5e-33-I_Syy.*r_B.*rolld.*t9.*t18.*t19.*6.981317007977318e-1+I_Szz.*r_B.*rolld.*t9.*t18.*t19.*6.981317007977318e-1+A_S.*C_D_S.*r_B.*t12.*t16.*t18.*t52.*6.125e-1+A_S.*C_D_S.*p_B.*t9.*t10.*t52.*z_SC.*8.544375e-1-A_S.*C_D_S.*t9.*t10.*t52.*v_B.*z_SC.*6.125e-1-I_Syy.*p_B.*rolld.*t9.*t10.*t17.*t19.*6.981317007977318e-1+I_Szz.*p_B.*rolld.*t9.*t10.*t17.*t19.*6.981317007977318e-1+I_Syy.*r_B.*rolld.*t9.*t18.*t19.*t66.*5.0e-33-I_Syy.*r_B.*rolld.*t9.*t18.*t19.*t67.*5.0e-33-I_Szz.*r_B.*rolld.*t9.*t18.*t19.*t66.*5.0e-33+I_Szz.*r_B.*rolld.*t9.*t18.*t19.*t67.*5.0e-33-p_B.*rolld.*t9.*t10.*t12.*t17.*t19.*1.605702911834783+A_S.*C_D_S.*p_B.*t10.*t12.*t16.*t17.*t52.*6.125e-1-A_S.*C_D_S.*q_B.*t9.*t12.*t17.*t19.*t52.*6.125e-1];
