disp 'InitialStates'
for i=1:length(fit.InitialStates)
    fprintf('\t%s: %d\tFixed: %d\n', fit.InitialStates(i).Name, fit.InitialStates(i).Value, fit.InitialStates(i).Fixed);
end
disp 'Parameters'
covar = diag(fit.Report.Parameters.FreeParCovariance);
for i=1:length(fit.Parameters)
	fprintf('\t%s: %d\n', fit.Parameters(i).Name, fit.Parameters(i).Value);
end

disp 'Simulating'
simu = sim(fit, data);
% Yaw gets wrapped, I don't want it wrapped!
simu.y(:,6) = unwrap(simu.y(:,6));
data_unwound = data;
data_unwound.y(:,6) = unwrap(data_unwound.y(:,6));

% See 
leg = @() legend('Experimental Data', 'Model Prediction');
x_range=[000 100];

% Plot X, Y, Z
figure(1)
set(gcf, 'Color', 'w');
units = {'$x_S$ (m)', '$y_S$ (m)', '$z_S$ (m)'};
for ii=1:3
    subplot(3,1,ii);
    hold on
    plot(data_unwound.SamplingInstants, data_unwound.y(:,ii), 'LineWidth',2);
    plot(simu.SamplingInstants, simu.y(:,ii));
    xlabel('Time (seconds)','interpreter','latex')
    ylabel(units(ii),'interpreter','latex')
    xlim(x_range);
end
% Titling graph is just titling first subplot
subplot(3,1,1);
title('Payload Position','interpreter','latex')
subplot(3,1,3); 
% Legend needs to be drawn on 3rd, so it isn't occluded by other graphs
l=leg();
set(l,'interpreter','latex')
set(gcf, 'Position', get(gcf, 'Position').*[1, 1, 2, 1])

% Plot Attitude
figure(2)
set(gcf, 'Color', 'w');
units = {'$\phi_S$ (rad)', '$\theta_S$ (rad)', '$\psi_S$ (rad)'};
for ii=4:6
    subplot(3,1,ii-3);
    hold on
    plot(data_unwound.SamplingInstants, data_unwound.y(:,ii), 'LineWidth',2);
    plot(simu.SamplingInstants, simu.y(:,ii));
    xlabel('Time (seconds)','interpreter','latex')
    ylabel(units(ii-3),'interpreter','latex')
    xlim(x_range);
end
% Titling graph is just titling first subplot
subplot(3,1,1);
title('Payload Attitude','interpreter','latex')
subplot(3,1,3); 
% Legend needs to be drawn on 3rd, so it isn't occluded by other graphs
l=leg();
set(l,'interpreter','latex')
set(gcf, 'Position', get(gcf, 'Position').*[1, 1, 2, 1])

% UVW
figure(3)
set(gcf, 'Color', 'w');
units = {'$u_S$ (m/s)', '$v_S$ (m/s)', '$w_S$ (m/s)'};
for ii=7:9
    subplot(3,1,ii-6);
    hold on
    plot(data_unwound.SamplingInstants, data_unwound.y(:,ii), 'LineWidth',2);
    plot(simu.SamplingInstants, simu.y(:,ii));
    xlabel('Time (seconds)','interpreter','latex')
    ylabel(units(ii-6),'interpreter','latex')
    xlim(x_range);
end
% Titling graph is just titling first subplot
subplot(3,1,1);
title('Payload Body Frame Translation Speeds','interpreter','latex')
% Set u range so it's more accurate 
ylim([0 15]);
subplot(3,1,3); 
% Legend needs to be drawn on 3rd, so it isn't occluded by other graphs
l=leg();
set(l,'interpreter','latex')
set(gcf, 'Position', get(gcf, 'Position').*[1, 1, 2, 1])

% PQR
figure(4)
set(gcf, 'Color', 'w');
units = {'$p_S$ (rad/s)', '$q_S$ (rad/s)', '$r_S$ (rad/s)'};
for ii=10:12
    subplot(3,1,ii-9);
    hold on
    plot(data_unwound.SamplingInstants, data_unwound.y(:,ii), 'LineWidth',2);
    plot(simu.SamplingInstants, simu.y(:,ii));
    xlabel('Time (seconds)','interpreter','latex')
    ylabel(units(ii-9),'interpreter','latex')
    xlim(x_range);
end
% Titling graph is just titling first subplot
subplot(3,1,1);
title('Payload Body Frame Rotation Speeds','interpreter','latex')
subplot(3,1,3); 
% Legend needs to be drawn on 3rd, so it isn't occluded by other graphs
l=leg();
set(l,'interpreter','latex')
set(gcf, 'Position', get(gcf, 'Position').*[1, 1, 2, 1])

% Plot controls
figure(5)
set(gcf, 'Color', 'w');
units = {'Thrust Input \%', 'Roll Input \%'};
for ii=1:2
    subplot(2,1,ii);
    hold on
    plot(data_unwound.SamplingInstants, data_unwound.u(:,ii), 'LineWidth',2);
    xlabel('Time (seconds)','interpreter','latex')
    ylabel(units(ii),'interpreter','latex')
    xlim(x_range);
end
% Titling graph is just titling first subplot
subplot(2,1,1);
title('Control Inputs','interpreter','latex')
subplot(2,1,2); 
% Legend needs to be drawn on 3rd, so it isn't occluded by other graphs
l=legend('Control Input Percentage');
set(l,'interpreter','latex')
set(gcf, 'Position', get(gcf, 'Position').*[1, 1, 2, 1])