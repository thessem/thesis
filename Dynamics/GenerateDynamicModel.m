clear
restoredefaultpath;
clear all;
close all;
clear functions;
bdclose('all');
fclose('all');
rehash

tic
folder  = './Functions/7DoF/';
disp 'Loading Symbol table'
run(strcat(folder, 'Symbols'))

disp 'Loading equations into memory'
% We run everything in the subdirectory
list    = dir(fullfile(folder, '*Symb.m'));
nFile   = length(list);
for k = 1:nFile
  file = list(k).name;
  % Don't load the keys
  if ~any(strfind(file, 'Key'))
    fprintf('\tLoading %s\n', file);
    run(fullfile(folder, file));
  end
end

disp 'Simplifying symbolic equations'
disp '    Simplifying KinDiff'
KinDiff = simplify(KinDiff);
disp '    Simplifying M'
M = simplify(M);
disp '    Simplifying F'
F = simplify(vpa(F));

disp 'Solving symbolic equations'
disp '    Inverting M'
M_inv = M^-1;
disp '    Forming RHS'
RHS = M_inv*F;
% disp '    Converting RHS to VPA'
% RHS = vpa(RHS);
% disp '    Simplifying RHS'
% RHS = simplify(RHS);

%disp 'Saving Symbolic Equations'
%save('simplified.mat', 'KinDiff', 'M', 'F', 'SpeedCon');

disp 'Generating and writing functions'
disp '    Loading Functions'
disp '    Generating M'
matlabFunction(M,'File',strcat(folder, 'M'), 'Vars', {control_sym, q_ind_sym, param_sym});
disp '    Generating F'
matlabFunction(F,'File',strcat(folder, 'F'), 'Vars', {control_sym, q_ind_sym, u_ind_sym, param_sym});
disp '    Generating RHS'
matlabFunction(RHS,'File',strcat(folder, 'RHS'), 'Vars', {control_sym, q_ind_sym, u_sym, param_sym});
disp '    Generating KinDiff'
matlabFunction(KinDiff,'File',strcat(folder, 'KinDiff'), 'Vars', {control_sym, q_ind_sym, u_ind_sym, param_sym});
disp '    Generating SpeedCon'
matlabFunction(SpeedCon,'File',strcat(folder, 'SpeedCon'), 'Vars', {control_sym, q_ind_sym, u_ind_sym, param_sym});
disp '    Generating RotMat'
matlabFunction(RotMat,'File',strcat(folder, 'RotMat'), 'Vars', {control_sym, q_ind_sym, param_sym});

disp 'Clearing temporary functions from memory'
clear M F KinDiff SpeedCon RHS RotMat
toc