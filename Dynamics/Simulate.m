addpath DynamicModel

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        Setting Parameters                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ParName = {'z_CS';                       ...
%            'z_MS';                       ...
%            'I_Sxx'; 'I_Syy'; 'I_Szz';    ...
%            'I_Bxx'; 'I_Byy'; 'I_Bzz';    ...
%            'A_S';                        ...
%            'theta_B';                    ...
%            'C_t';                        ... 
%            'C_D_S';                      ... 
%            'C_D0_B';                     ...
%            'C_L0_B';                     ... 
%            'C_Dalpha_B';                 ... 
%            'C_Lalpha_B';                 ...
%            'C_l_p';                      ...
%            'C_m_q';                      ...
%            'C_m_0';                      ...
%            'C_n_r'};
ParValue = [-0.161;                 ...
            -0.1;                   ...
            0.2; 0.2; 0.1;          ...
            .031; .020; .040;       ...
            0.5;                    ...
            deg2rad(-20);            ...
            30;                     ... 
            .15;                      ... 
            0.4;                   ...
            0.8;                   ... 
            0.4;                   ... 
            0.68;                   ...
            -.9;                 ...
            -.2;                 ...
            -0.10;                   ...
            -0.3]';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        Simulation Settings                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t_final = 120;
dt = 0.005;
tspan = 0:dt:t_final;

% Setting initial Conditions
qIndInit = 1*[0, 0, 0, 0, 6.054166e-01, 0]';
uIndInit = 1*[7.270196e+00, 0, 3.134399e+00, 0, 0, 0]';
y0 = [qIndInit; uIndInit];

% Construct control vector
% If this is meant to be piece by piece, stretch it out with repmat
control = [repmat([1*0.5, 1*0.0], floor(length(tspan)/4), 1);
           repmat([1*0.5, 1*0.25], floor(length(tspan)/4), 1);
           repmat([1*0.5, 1*0.50], floor(length(tspan)/4), 1);
           repmat([1*0.5, 1*0.75], ceil(length(tspan)/4), 1)];

% Add the calculated differential terms
control = [control, [0; diff(control(:,2))]];
control = [control, [0; diff(control(:,3))]];

% ode function is y' = f(t,y)
odefun = @(t, y) [KinDiff(control(round(t/dt+1),:), y(1:6)', y(7:12)', ParValue); (M(control(round(t/dt+1),:), y(1:6)', ParValue)\F(control(round(t/dt+1),:), y(1:6)', y(7:12)', ParValue))];

% Perform integration
display 'Simulating'
[T, Y] = ode45(odefun, tspan, y0);

% Split up variables and calculate dependents
% Calculate in terms of S
display 'Calculating Dependent Variables'
coordinates = zeros(length(tspan),6);
speeds = zeros(length(tspan),6);
for ii=1:length(tspan)
    % Apparently roll is 3 and pitch is 2. Check this!
    coordinates(ii,:) = [CoordCon(control(ii,:), Y(ii,1:6), ParValue)', fliplr(rotm2eul(RotMat(control(ii,:), Y(ii,1:6), ParValue)))];
    speeds(ii,:) = SpeedCon(control(ii,:), Y(ii,1:6), Y(ii,7:12), ParValue)';
end

display 'Generating Graphs'

% Don't export here, and make sure hold is on so that we can add multiple
% tests
% If doing multiple tests set the legend below
% Done as an anon function because I couldn't figure out a better way
leg = @() legend('Unpowered/$0.5$ Turn', '$0.3$ Power/$0.5$ Turn', '$0.6$ Power/$0.5$ Turn', '$0.6$ Power/$0.1$ Turn');

figure(1)
hold on
title('Overhead Path View','interpreter','latex')
set(gcf, 'Color', 'w');
plot(coordinates(:,1),coordinates(:,2))
xlabel('North Distance $x_S$ (metres)','interpreter','latex')
ylabel('East Distance $y_S$ (metres)','interpreter','latex')
l=leg();
set(l,'interpreter','latex')

figure(2)
hold on
title('Altitude Path','interpreter','latex')
set(gcf, 'Color', 'w');
plot(T,-coordinates(:,3))
xlabel('Time (seconds)','interpreter','latex')
ylabel('Altitude $-z_S$ (metres)','interpreter','latex')
l=leg();
set(l,'interpreter','latex')

figure(3)
hold on
title('Parafoil Roll','interpreter','latex')
set(gcf, 'Color', 'w');
plot(T, Y(:,4))
xlabel('Time (seconds)','interpreter','latex')
ylabel('Parafoil Roll $\phi_B$ (rad)','interpreter','latex')
l=leg();
set(l,'interpreter','latex')

figure(4)
hold on
title('Parafoil Path','interpreter','latex')
set(gcf, 'Color', 'w');
plot3(Y(:,1), Y(:,2), -Y(:,3))
xlabel('North Distance $x_S$ (metres)','interpreter','latex')
ylabel('East Distance $x_S$ (metres)','interpreter','latex')
zlabel('Altitude $-z_S$ (metres)','interpreter','latex')
l=leg();
set(l,'interpreter','latex')
axis equal tight

% Plot controls
figure(5)
set(gcf, 'Color', 'w');
units = {'Thrust Input \%', 'Roll Input \%'};
for ii=1:2
    subplot(2,1,ii);
    hold on
    plot(T, control(:,ii));
    xlabel('Time (seconds)','interpreter','latex')
    ylabel(units(ii),'interpreter','latex')
    xlim([0 max(T)]);
end
% Titling graph is just titling first subplot
subplot(2,1,1);
title('Control Inputs','interpreter','latex')
subplot(2,1,2); 
% Legend needs to be drawn on 3rd, so it isn't occluded by other graphs
l=legend('Control Input Percentage');
set(l,'interpreter','latex')