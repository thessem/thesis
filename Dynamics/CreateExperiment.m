
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Parse Experimental Data                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('../FlightLogs/Data/14-08-2015-session2.mat');
% Resample
frequency = 0.1;
times_r = 300:frequency:300+100;
%times_r = 276:.1:282;
[times_u, idx] = unique(times);
data = [coordinates(:,1:6), speeds(:,1:6)];
data_r = zeros(length(times_r), 12);
control_r = zeros(length(times_r), 4);
for ii = 1:12
    data_r(:,ii) = interp1(times_u, data(idx,ii), times_r');
end
for ii = 1:4
    control_r(:,ii) = interp1(times_u, control(idx,ii), times_r');
end

data = iddata(data_r, control_r, frequency);
z.Name = '14-08-2015-session2';

InputName = {'Throttle';                ...
             'Roll';
             'Roll Velocity'
             'Roll Acceleration'};
InputUnit = {'Percentage';    ...
             'Percentage';
             'Percentage/s';
             'Percentage/s^2'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Construct Model                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% These variables are the state variables of the simulation
StateName  = {'x_B';      ...
              'y_B';      ...
              'z_B';      ...
              '\phi_B';   ...
              '\theta_B'; ...
              '\psi_B';   ...
              'u_B';      ...
              'v_B';      ...
              'w_B';      ...
              'p_B';      ... 
              'q_B';      ... 
              'r_B'};
StateUnit  = {'m';        ...
              'm';        ...
              'm';        ...
              'rad';      ...
              'rad';      ...
              'rad';      ...
              'm/s';    ...
              'm/s';    ...
              'm/s';    ...
              'rad/s';      ...
              'rad/s';      ...
              'rad/s'};

% We perform system identification against the speeds, not coordinates
OutputName = {'x_S'; 'y_S'; 'z_S'; 'phi_S'; 'theta_S'; 'psi_S'; 'u_S'; 'v_S'; 'w_S'; 'p_S'; 'q_S'; 'r_S'};
OutputUnit = StateUnit(1:12);

% All parameters of the model, including constants, since we want to be able to
% add a degree of experimental error to the fitting
ParName = {'z_CS';                       ...
           'z_MS';                       ...
           'I_Sxx'; 'I_Syy'; 'I_Szz';    ...
           'I_Bxx'; 'I_Byy'; 'I_Bzz';    ...
           'A_S';                        ...
           'theta_B';                    ...
           'C_t';                        ... 
           'C_D_S';                      ... 
           'C_D0_B';                     ...
           'C_L0_B';                     ... 
           'C_Dalpha_B';                 ... 
           'C_Lalpha_B';                 ...
           'C_l_p';                      ...
           'C_m_q';                      ...
           'C_m_0';                      ...
           'C_n_r'};
ParUnit  = {'m';                             ...
            'm';                             ...
            'kg m^2'; 'kg m^2'; 'kg m^2';    ...
            'kg m^2'; 'kg m^2'; 'kg m^2';    ...
            'm^2';                           ...
            'rad';                           ...
            'N';                             ... 
            'dimensionless';                 ... 
            'dimensionless';                 ...
            'dimensionless';                 ... 
            'dimensionless';                 ...
            'dimensionless';                 ...
            'dimensionless';                 ...
            'dimensionless';                 ...
            'dimensionless';                 ...
            'dimensionless'};
ParValue = {-0.161;                 ...
            -0.1;                   ...
            0.2; 0.2; 0.1;          ...
            .031; .020; .040;       ...
            0.5;                    ...
            deg2rad(-20);            ...
            30;                     ... 
            .15;                      ... 
            0.4;                   ...
            0.8;                   ... 
            0.4;                   ... 
            0.68;                   ...
            -.9;                 ...
            -.2;                 ...
            -0.10;                   ...
            -0.3};
        
ParMin   = {-.4;                     ...
            -.2;                     ...
            eps(0); eps(0);eps(0);  ...
            eps(0); eps(0);eps(0);  ...
            eps(0);                 ...
            -deg2rad(90);           ...
            eps(0);                 ... 
            eps(0);                 ... 
            eps(0);                 ...
            eps(0);                 ... 
            eps(0);                 ... 
            eps(0);                 ...
            -4;                   ...
            -4;                   ...
            -1;                   ...
            -1};                    
ParMax   = {0;                      ...
            .1;                      ...
            Inf; Inf; Inf;          ...
            Inf; Inf; Inf;          ...
            3;                      ...
            deg2rad(90);            ...
            50;                    ... 
            5;                    ... 
            3;                    ...
            3;                    ... 
            3;                    ... 
            3;                    ...
            4;%-eps(0);                    ...
            4;%-eps(0);                    ...
            1;                    ...
            1};%.05}; 

qInit = data_r(1,1:6);
uIndInit = 0*data_r(1,7:12);
StateInit  = [num2cell(qInit), num2cell(uIndInit)]';
        
FileName      = 'Paramotor_m';
Order         = [12 4 12]; % Model orders [ny nu nx].
Parameters    = struct('Name', ParName, 'Unit', ParUnit, 'Value', ParValue, ...
                       'Minimum', ParMin, 'Maximum', ParMax, 'Fixed', false);
InitialStates = struct('Name', StateName, 'Unit', StateUnit, 'Value', StateInit, ...
                       'Minimum', -Inf, 'Maximum', Inf, 'Fixed', false);
% Time-continuous system
Ts            = 0;                       
paramotor_nlgr = idnlgrey(FileName, Order, Parameters, InitialStates, Ts, ...
                          'Name', 'paramotor', 'InputName', InputName, ...
                          'InputUnit', InputUnit, 'OutputName', OutputName, ...
                          'OutputUnit', OutputUnit,  'TimeUnit', 's');
%paramotor_nlgr.SimulationOptions.MaxStep = 0.01;
paramotor_nlgr.Parameters = fit.Parameters;
paramotor_nlgr.InitialStates = fit.InitialStates;

% Set fixed sense
for i=1:12
    paramotor_nlgr.InitialStates(i).Fixed = false;
end
for i=1:20
    paramotor_nlgr.Parameters(i).Fixed = true;
end
paramotor_nlgr.Parameters(9).Fixed = true;

opt = nlgreyestOptions;
opt.Display = 'on';
opt.SearchOption.MaxIter = 500;
%opt.SearchOption.TolFun = 1e-10;
% u_S, v_S, w_S, p_S, q_S, r_S, phi_S, theta_S, psi_S
opt.OutputWeight = diag([.0001, .0001, .001, 1, 1, 0.0, 1, .1, 1, 1, .1, 1]);
opt.SearchOption.Advanced.Display = 'on';
%opt.Regularization.Lambda = 1e-4;

opt.Regularization.Nominal = 'model';
% opt.Regularization.R = [10*ones([12,1]);
%                         100;                 ...
%                         10;                   ...
%                         10; 10; 10;          ...
%                         10; 10; 10;       ...
%                         10;                    ...
%                         1;            ...
%                         10;                     ... 
%                         1;                      ... 
%                         1;                   ...
%                         1;                   ... 
%                         1;                   ... 
%                         1;                   ...
%                         1;                 ...
%                         1;                 ...
%                         1;                   ...
%                         1];

data.InputName = paramotor_nlgr.InputName;
data.InputUnit = paramotor_nlgr.InputUnit;
data.OutputName = paramotor_nlgr.OutputName;
data.OutputUnit = paramotor_nlgr.OutputUnit;
data.TimeUnit = 's';

%fit_300_400s_verification = nlgreyest(paramotor_nlgr, data, opt);
%save('final_fit.mat', 'fit_200_300s', 'fit_200_300s_noCoords', 'fit_300_400s_verification')
%save('fit.mat', 'fit_initial', 'fit_55_65s_params', 'fit_55_105s', 'fit_55_105s_noR', 'fit_55_125s', 'fit_55_125s_noR', 'fit_55_125s_initial', 'fit_150_200s', 'fit_200_300s');
