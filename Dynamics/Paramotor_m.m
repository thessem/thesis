function [dx, y] = Paramotor_m(~, x, u, varargin)
    % x_BI, y_BI, z_BI, phi_B, theta_B, psi_B
    coordinates = x(1:6)';
    parameters = [varargin{1:end-1}];    
    % u_B, v_B, w_B, p_B, q_B, r_B, u_S, v_S, w_S, p_S, q_S, r_S
    ind_speeds = x(7:12)';
    dep_speeds = SpeedCon(u, coordinates, x(7:12)', parameters)';
    
    % x_B, y_B, z_B, phi_B, theta_B, psi_B, u_B, v_B, w_B, p_B, q_B, r_B, u_S, v_S, w_S, p_S, q_S, r_S
    dx = KinDiff(u, coordinates, ind_speeds, parameters);
    dx = [dx; M(u, coordinates, [varargin{1:end-1}])\F(u, x(1:6)', ind_speeds, parameters)];
    
    % We need to calculate the roll, pitch and yaw of S
    abs_S = rotm2eul(RotMat(u, coordinates, parameters));
    % Apparently roll is 3 and pitch is 2. Check this!
    abs_S = [CoordCon(u, coordinates, parameters)', abs_S(3), abs_S(2), abs_S(1)];
    
    % u_S, v_S, w_S, p_S, q_S, r_S, phi_S, theta_S, psi_S
    y = [abs_S'; dep_speeds'];
end