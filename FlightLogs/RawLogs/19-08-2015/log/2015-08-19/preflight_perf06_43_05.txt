PERFORMANCE COUNTERS PRE-FLIGHT

sd write: 0 events, 0 overruns, 0us elapsed, 0us avg, min 0us max 0us 0.000us rms
mavlink_txe: 24 events
mavlink_el: 62761 events, 0 overruns, 6169495us elapsed, 98us avg, min 30us max 1301015us 5211.916us rms
navigator: 35831 events, 0 overruns, 1659946us elapsed, 46us avg, min 9us max 807764us 4267.973us rms
fw l1 control: 16463 events, 0 overruns, 453165us elapsed, 27us avg, min 24us max 98us 7.699us rms
fw att control yaw nonfinite input: 0 events
fw att control pitch nonfinite input: 0 events
fw att control roll nonfinite input: 0 events
fw att control nonfinite output: 0 events
fw att control nonfinite input: 0 events
fw att control: 36582 events, 0 overruns, 1356611us elapsed, 37us avg, min 24us max 280us 18.626us rms
ekf_att_pos_reset: 0 events
ekf_att_pos_aspd_upd: 27510 events, 12018us avg, min 8681us max 39333us 4250.689us rms
ekf_att_pos_baro_upd: 17876 events, 18494us avg, min 8697us max 74543us 7992.853us rms
ekf_att_pos_gps_upd: 1653 events, 199914us avg, min 99084us max 324311us 24187.518us rms
ekf_att_pos_mag_upd: 25704 events, 12864us avg, min 8576us max 47442us 4511.742us rms
ekf_att_pos_gyro_upd: 73402 events, 4505us avg, min 11us max 30299us 4493.540us rms
ekf_att_pos_est_interval: 36701 events, 9010us avg, min 516us max 30311us 146.658us rms
ekf_att_pos_estimator: 36590 events, 0 overruns, 71652973us elapsed, 1958us avg, min 810us max 2920us 415.890us rms
io latency: 36498 events, 0 overruns, 207896136us elapsed, 5696us avg, min 1136us max 14510us 2142.074us rms
io write: 0 events, 0 overruns, 0us elapsed, 0us avg, min 0us max 0us 0.000us rms
io update: 36561 events, 0 overruns, 216455024us elapsed, 5920us avg, min 1683us max 104451us 4800.019us rms
sensor task update: 82833 events, 0 overruns, 5438010us elapsed, 65us avg, min 40us max 12222us 64.405us rms
hmc5883_conf_errors: 1 events
hmc5883_range_errors: 0 events
hmc5883_buffer_overflows: 27066 events
hmc5883_comms_errors: 0 events
hmc5883_read: 27069 events, 0 overruns, 53247853us elapsed, 1967us avg, min 870us max 26800us 1080.387us rms
l3gd20_duplicates: 215588 events
l3gd20_bad_registers: 0 events
l3gd20_errors: 0 events
l3gd20_read: 466551 events, 0 overruns, 11964189us elapsed, 25us avg, min 20us max 30us 4.523us rms
ctrl_latency: 0 events, 0 overruns, 0us elapsed, 0us avg, min 0us max 0us 0.000us rms
sys_latency: 0 events, 0 overruns, 0us elapsed, 0us avg, min 0us max 0us 0.000us rms
mpu6000_duplicates: 83678 events
mpu6000_reset_retries: 0 events
mpu6000_good_transfers: 333336 events
mpu6000_bad_registers: 0 events
mpu6000_bad_transfers: 0 events
mpu6000_read: 417041 events, 0 overruns, 18554976us elapsed, 44us avg, min 29us max 58us 7.677us rms
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 1 events
adc_samples: 165980 events, 0 overruns, 462083us elapsed, 2us avg, min 2us max 3us 0.411us rms
ms5611_buffer_overflows: 18092 events
ms5611_comms_errors: 5 events
ms5611_measure: 24147 events, 0 overruns, 33309735us elapsed, 1379us avg, min 215us max 33411us 1209.251us rms
ms5611_read: 24144 events, 0 overruns, 43258730us elapsed, 1791us avg, min 594us max 15508us 1142.091us rms
eeprom_errs: 0 events
eeprom_rst: 46 events
eeprom_trans: 572 events, 0 overruns, 2266956us elapsed, 3963us avg, min 59us max 21962us 4725.127us rms
