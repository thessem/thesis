PERFORMANCE COUNTERS PRE-FLIGHT

sd write: 0 events, 0 overruns, 0us elapsed, 0us avg, min 0us max 0us 0.000us rms
mavlink_txe: 15 events
mavlink_el: 61676 events, 0 overruns, 6534321us elapsed, 105us avg, min 30us max 1062676us 5053.261us rms
navigator: 35205 events, 0 overruns, 1669371us elapsed, 47us avg, min 9us max 798881us 4258.488us rms
fw l1 control: 16206 events, 0 overruns, 461811us elapsed, 28us avg, min 24us max 121us 8.992us rms
fw att control yaw nonfinite input: 0 events
fw att control pitch nonfinite input: 0 events
fw att control roll nonfinite input: 0 events
fw att control nonfinite output: 0 events
fw att control nonfinite input: 0 events
fw att control: 36009 events, 0 overruns, 1240774us elapsed, 34us avg, min 24us max 148us 14.487us rms
ekf_att_pos_reset: 0 events
ekf_att_pos_aspd_upd: 27081 events, 12017us avg, min 2552us max 28739us 4250.067us rms
ekf_att_pos_baro_upd: 17591 events, 18502us avg, min 8618us max 62994us 8022.787us rms
ekf_att_pos_gps_upd: 1627 events, 199795us avg, min 54041us max 360451us 16370.408us rms
ekf_att_pos_mag_upd: 25223 events, 12905us avg, min 8609us max 72074us 4529.148us rms
ekf_att_pos_gyro_upd: 72254 events, 4505us avg, min 11us max 28724us 4493.898us rms
ekf_att_pos_est_interval: 36128 events, 9010us avg, min 2554us max 28741us 165.096us rms
ekf_att_pos_estimator: 36017 events, 0 overruns, 70237133us elapsed, 1950us avg, min 790us max 3074us 416.527us rms
io latency: 35941 events, 0 overruns, 204203623us elapsed, 5681us avg, min 1707us max 14870us 2140.984us rms
io write: 0 events, 0 overruns, 0us elapsed, 0us avg, min 0us max 0us 0.000us rms
io update: 35980 events, 0 overruns, 212388355us elapsed, 5902us avg, min 1682us max 104389us 4799.941us rms
sensor task update: 81389 events, 0 overruns, 5282905us elapsed, 64us avg, min 40us max 12228us 64.704us rms
hmc5883_conf_errors: 1 events
hmc5883_range_errors: 0 events
hmc5883_buffer_overflows: 26559 events
hmc5883_comms_errors: 0 events
hmc5883_read: 26562 events, 0 overruns, 52375065us elapsed, 1971us avg, min 870us max 49385us 1099.628us rms
l3gd20_duplicates: 211897 events
l3gd20_bad_registers: 0 events
l3gd20_errors: 0 events
l3gd20_read: 458475 events, 0 overruns, 11756647us elapsed, 25us avg, min 20us max 30us 4.523us rms
ctrl_latency: 0 events, 0 overruns, 0us elapsed, 0us avg, min 0us max 0us 0.000us rms
sys_latency: 0 events, 0 overruns, 0us elapsed, 0us avg, min 0us max 0us 0.000us rms
mpu6000_duplicates: 82114 events
mpu6000_reset_retries: 0 events
mpu6000_good_transfers: 327681 events
mpu6000_bad_registers: 0 events
mpu6000_bad_transfers: 0 events
mpu6000_read: 409813 events, 0 overruns, 18233897us elapsed, 44us avg, min 29us max 58us 7.672us rms
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 1 events
adc_samples: 163090 events, 0 overruns, 451877us elapsed, 2us avg, min 2us max 3us 0.420us rms
ms5611_buffer_overflows: 17771 events
ms5611_comms_errors: 1 events
ms5611_measure: 23706 events, 0 overruns, 32606543us elapsed, 1375us avg, min 216us max 28760us 1196.515us rms
ms5611_read: 23704 events, 0 overruns, 42781464us elapsed, 1804us avg, min 593us max 28746us 1159.519us rms
eeprom_errs: 0 events
eeprom_rst: 82 events
eeprom_trans: 1088 events, 0 overruns, 4307696us elapsed, 3959us avg, min 59us max 26282us 4772.854us rms
