flight_logs_dir = '..\Data\';
filename = '14-08-2015-session2';

% I use this for searching for the right column later, I don't want to hard
% code indexes
fid = fopen(strcat(flight_logs_dir, filename, '.csv'));
a = textscan(fid, '%s', 1);
names = strsplit(a{1}{1},',');

% Read in csv
csv = csvread([flight_logs_dir, filename, '.csv'], 1);

% Time is converted from cpu microseconds to seconds since start
times = csv(:,ismember(names,'TIME_StartTime'));
times = (times - times(1))/1e6;

% Coordinates are [x_S, y_S, z_S, ATT_Roll, ATT_Pitch, ATT_Yaw]
LPOS_X = csv(:,ismember(names,'LPOS_X'));
LPOS_Y = -csv(:,ismember(names,'LPOS_Y'));
LPOS_Z = csv(:,ismember(names,'LPOS_Z'));
ATT_Roll = -smooth(csv(:,ismember(names,'ATT_Roll')), 100, 'loess');
ATT_Pitch = smooth(csv(:,ismember(names,'ATT_Pitch')), 100, 'loess');
ATT_Yaw = -smooth(unwrap(csv(:,ismember(names,'ATT_Yaw'))), 100, 'loess');
coordinates = [LPOS_X, LPOS_Y, LPOS_Z, ATT_Roll, ATT_Pitch, ATT_Yaw];

% Speeds are [p_S, q_S, r_S, u_S, v_S, w_S]
% These must be converted from coordinates using some equations
speeds = zeros(size(coordinates));
LPOS_VX = smooth(csv(:,ismember(names,'LPOS_VX')),100,'loess');
LPOS_VY = -smooth(csv(:,ismember(names,'LPOS_VY')),100,'loess');
LPOS_VZ = smooth(csv(:,ismember(names,'LPOS_VZ')),100,'loess');
ATT_RollRate = -smooth(csv(:,ismember(names,'ATT_RollRate')),100,'loess');
ATT_PitchRate = smooth(csv(:,ismember(names,'ATT_PitchRate')),100,'loess');
ATT_YawRate = -smooth(csv(:,ismember(names,'ATT_YawRate')),100,'loess');

% Account for wind
WIND_X = csv(:,ismember(names,'WIND_X'));
WIND_Y = -csv(:,ismember(names,'WIND_Y'));
LPOS_VX = LPOS_VX - WIND_X;
LPOS_VY = LPOS_VY - WIND_Y;
for ii = 1:length(times)
    dcm = [[cos(ATT_Yaw(ii))*cos(ATT_Pitch(ii)), sin(ATT_Roll(ii))*sin(ATT_Pitch(ii))*cos(ATT_Yaw(ii)) - sin(ATT_Yaw(ii))*cos(ATT_Roll(ii)),  sin(ATT_Roll(ii))*sin(ATT_Yaw(ii)) + sin(ATT_Pitch(ii))*cos(ATT_Roll(ii))*cos(ATT_Yaw(ii))];
           [ sin(ATT_Yaw(ii))*cos(ATT_Pitch(ii)), sin(ATT_Roll(ii))*sin(ATT_Yaw(ii))*sin(ATT_Pitch(ii)) + cos(ATT_Roll(ii))*cos(ATT_Yaw(ii)), -sin(ATT_Roll(ii))*cos(ATT_Yaw(ii)) + sin(ATT_Yaw(ii))*sin(ATT_Pitch(ii))*cos(ATT_Roll(ii))];
           [                 -sin(ATT_Pitch(ii)),                                                       sin(ATT_Roll(ii))*cos(ATT_Pitch(ii)),                                                        cos(ATT_Roll(ii))*cos(ATT_Pitch(ii))]]';
    speeds(ii,1:3) = (dcm*[LPOS_VX(ii); LPOS_VY(ii); LPOS_VZ(ii)])';
    
    speeds(ii,4:6) = [ATT_RollRate(ii)-sin(ATT_Pitch(ii))*ATT_YawRate(ii); 
                      cos(ATT_Roll(ii))*ATT_PitchRate(ii) + sin(ATT_Roll(ii))*cos(ATT_Pitch(ii))*ATT_YawRate(ii); 
                      cos(ATT_Pitch(ii))*cos(ATT_Roll(ii))*ATT_YawRate(ii) - sin(ATT_Roll(ii))*ATT_PitchRate(ii)]';
end
coordinates(:,6) = mod(coordinates(:,6)-coordinates(1,6), 2*pi);

% Control inputs [f_S_t, phi_B, phi_Bd, phi_Bdd]
RC_Ch0 = csv(:,ismember(names,'RC_Ch0'));
RC_Ch2 = smooth(csv(:,ismember(names,'RC_Ch2')),100,'loess');
control = zeros([length(times), 4]);

% Thrust
control(:,1) = RC_Ch2;
% Quickly fix a trim problem
control(:,2) = RC_Ch0 + 0.228 - 0.075;
% These are two derivatives
control(:,3) = [0; smooth(diff(control(:,2)),100,'loess')];
control(:,4) = [0; smooth(diff(control(:,3)),100,'loess')];

save_filename = [flight_logs_dir, filename, '.mat'];
save(save_filename, 'times', 'control', 'coordinates', 'speeds');